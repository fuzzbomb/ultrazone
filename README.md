UltraZone module
================

This Drupal module provides empty hook implementations (mostly "alter" hooks)
where we typically might want to use `dpm()`, `print_r()`, `drush_print()`, &c.
for quick and dirty debugging.

The name is a play on "Alter Zone", and also pays homage to the Blake's 7 episode
"[Ultraworld](https://blakes7.fandom.com/wiki/Ultraworld_(episode))", in which
_an idiot hacks Core_ causing it to collapse.

Put your debugging messages here, and avoid commiting this module to your
git repo :-)