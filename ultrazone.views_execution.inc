<?php

/**
 * @file
 * Provides empty views_execution hook implementations to experiment with.
 *
 * views_query_substitutions()
 * views_form_substitutions()
 * views_pre_view()
 * views_pre_build()
 * views_post_build()
 * views_pre_execute()
 * views_post_execute()
 * views_pre_render()
 * views_post_render()
 * views_query_alter()
 */

use Drupal\Core\Language\LanguageInterface;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\ViewExecutable;
use \Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * Implements hook_views_query_substitutions().
 */
function ultrazone_views_query_substitutions(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_form_substitutions().
 */
function ultrazone_views_form_substitutions() {
}

/**
 * Implements hook_views_pre_view().
 */
function ultrazone_views_pre_view(ViewExecutable $view, $display_id, array &$args) {
  // dpm(__FUNCTION__);
  // dpm($view, __FUNCTION__);
  // dpm($display_id, '$display_id');
  // dpm($args, '$args');
}

/**
 * Implements hook_views_pre_build().
 */
function ultrazone_views_pre_build(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_post_build().
 */
function ultrazone_views_post_build(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_pre_execute().
 */
function ultrazone_views_pre_execute(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_post_execute().
 */
function ultrazone_views_post_execute(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_pre_render().
 */
function ultrazone_views_pre_render(ViewExecutable $view) {
  // dpm($view, __FUNCTION__);
}

/**
 * Implements hook_views_post_render().
 */
function ultrazone_views_post_render(ViewExecutable $view, &$output, CachePluginBase $cache) {
  // dpm(__FUNCTION__);
  // dpm($view, __FUNCTION__);
  // dpm($output, '$output');
  // dpm($cache, '$cache');
}

/**
 * Implements hook_views_query_alter().
 */
function ultrazone_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  // dpm($view, __FUNCTION__);
  // dpm($query, __FUNCTION__);
}
