<?php

namespace Drupal\ultrazone\EventSubscriber;

use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UltrazonePreRowSave.
 *
 * @package Drupal\ultrazone\EventSubscriber
 */
class UltrazonePreRowSave implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['migrate.pre_row_save'][] = ['preSave'];
    return $events;
  }

  /**
   * This only hits when an entity can be saved.
   *
   * In the case of rows which fail, this event does not fire.
   * e.g. if a file_copy process plugin is used, and the source file cannot be
   * found, the migrate.pre_row_save doesn't fire.
   *
   * @param \Drupal\migrate\Event\MigratePreRowSaveEvent $event
   *   The event fired.
   */
  public function preSave(MigratePreRowSaveEvent $event) {

    /** @var \Drupal\migrate\Plugin\Migration $migration */
    // $migration = $event->getMigration();
    if (function_exists('drush_print')) {
      drush_print(__CLASS__ . '::' . __FUNCTION__);
    }
    // print_r(get_class_methods($event->getRow()));
    // print_r($event->getRow());
    // print_r($event->getRow()->getDestination());

  }

}
